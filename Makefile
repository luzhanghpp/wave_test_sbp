SRC = OPERATORS.f90 SBP4.f90 WAVERUN.f90

OBJ = OPERATORS.o SBP4.o WAVERUN.o

FC = gfortran

LALIBS = -llapack -lblas

#
# executable
#
wave.x: $(OBJ)
	$(FC) -o wave.x $(OBJ) $(LALIBS)
#
$(OBJ): $(SRC)
	$(FC) -c $*.f90 -o $@
#
clean:
	rm *.o  *.mod
