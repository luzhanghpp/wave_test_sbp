PROGRAM WAVERUN
!
! use sbp4 with ghost points to solve the wave equation in 1d
!
USE OPERATORS
USE SBP4
!
IMPLICIT NONE
!
INTEGER, PARAMETER :: N = 21  ! the number of grid points without ghost points
DOUBLE PRECISION, PARAMETER :: a = 0.d0 ! left boundary
DOUBLE PRECISION, PARAMETER :: b = 1.d0 ! right bondary
DOUBLE PRECISION, DIMENSION(0:N+1) :: x,u,upre,utemp,uexac
DOUBLE PRECISION, DIMENSION(1:N) :: TGMU
DOUBLE PRECISION, DIMENSION(1:N,0:N+1) :: tildeG
DOUBLE PRECISION, DIMENSION(1) :: fa, gb, xa, xb
DOUBLE PRECISION :: t,dt,h,Tfinal,mu,error
INTEGER :: i,k,j,Nt
!
h = (b - a)/DBLE(N-1)
mu = 1.d0
x = 0.d0
u = 0.d0
upre = 0.d0
DO i = 1,N
  x(i) = (i-1)*h
END DO
x(0) = a - h
x(N+1) = b + h
!
CALL SBP_gp_matrix(tildeG,N,mu)
!
fa = 0.d0
gb = 0.d0
xa(1) = a
xb(1) = b
t = 0.d0
Tfinal = 1.d0
dt = 0.01d0
Nt = (Tfinal - t)/dt
CALL sol(u,x,t,N+2)
CALL sol(upre,x,t-dt,N+2)
DO i = 1,Nt
  TGMU = 0.d0
  utemp = u
  DO k = 1,N
    DO j = 0,N+1
      TGMU(k) = TGMU(k)+tildeG(k,j)*u(j)
    END DO
  END DO
  u(1:N) = 2.d0*u(1:N)-upre(1:N)+dt*dt/h/h*TGMU(1:N)
  upre = utemp
  ! left assume to be NBC
  CAll dsol(fa,xa,t+i*dt,1)
  u(0) = (12.d0*h*fa(1)-u(4)+6.d0*u(3)-18.d0*u(2)+10.d0*u(1))/(-3.d0)
  ! right assume to be DBC
  CALL sol(gb,xb,t+(i+1)*dt,1)
  u(N+1) = (h*h/dt/dt*(gb(1)-2.d0*u(N)+upre(N))-tildeG(N,N)*u(N)-tildeG(N,N-1)*u(N-1) &
           -tildeG(N,N-2)*u(N-2)-tildeG(N,N-3)*u(N-3)-tildeG(N,N-4)*u(N-4) &
           -tildeG(N,N-5)*u(N-5)-tildeG(N,N-6)*u(N-6)-tildeG(N,N-7)*u(N-7))/tildeG(N,N+1)
END DO
! compute the error of the given method
error = 0.d0
CALL sol(uexac,x,Tfinal,N+2)
DO i = 1,N
  error = error + (uexac(i)-u(i))**2
END DO
error = SQRT(error/N)
print *, error
!
CONTAINS
!
SUBROUTINE sol(fe,xe,te,m)
! reference solution
!
DOUBLE PRECISION, INTENT(IN) :: te
INTEGER, INTENT(IN) :: m ! the number of points that we actually have
DOUBLE PRECISION, DIMENSION(0:m-1), INTENT(IN) :: xe
DOUBLE PRECISION, DIMENSION(0:m-1), INTENT(OUT) :: fe
DOUBLE PRECISION :: pi
!
pi = 4.d0*atan(1.d0)
DO i = 1,m
  fe(i) = SIN(2.d0*pi*(xe(i)-te))
END DO
!
END SUBROUTINE sol
!
SUBROUTINE dsol(fe,xe,te,m)
! derivative of the  reference solution
!
DOUBLE PRECISION, INTENT(IN) :: te
INTEGER, INTENT(IN) :: m
DOUBLE PRECISION, DIMENSION(0:m-1), INTENT(IN) :: xe
DOUBLE PRECISION, DIMENSION(0:m-1), INTENT(OUT) :: fe
DOUBLE PRECISION :: pi
!
pi = 4.d0*atan(1.d0)
DO i = 1,m
  fe(i) = 2.d0*pi*COS(2.d0*pi*(xe(i)-te))
END DO
!
END SUBROUTINE dsol
!
END PROGRAM WAVERUN
