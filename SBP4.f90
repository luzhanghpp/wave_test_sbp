MODULE SBP4
!
! solve acoustic wave equation with SBP4 in one dimension
!
USE OPERATORS
!
IMPLICIT NONE
!
CONTAINS
!
SUBROUTINE SBP_gp_matrix(tildeG,N,mu)
! Construct the 4th order sbp operator for the second derivatives (with ghost point)                                                                  
! tildeG(mu) ~= (mu*u_x)_x
!
! the form of matrix tildeG:
!              |* . . . . . . . .                  |
!              |  . . . . . . . .                  |
!              |  . . . . . . . .                  |
!              |  . . . . . . . .                  |
!              |  . . . . . . . .                  |
!              |  . . . . . . . .                  |
!              |          + + + + +                |
!              |            + + + + +              |
!              |              + + + + +            |
!              |                + + + + +          |
!              |                  . . . . . . . .  |
!              |                  . . . . . . . .  |
!              |                  . . . . . . . .  |
!              |                  . . . . . . . .  |
!              |                  . . . . . . . .  |
!              |                  . . . . . . . . *|
! * is coefficients for ghost points
! the first and the last six rows (.) come from the Beta, and it gurantee the 2nd order SBP property
! the milddle rows (+), the actually rows are N-2*6, and it is the five point stencil for 2nd derivative
INTEGER, INTENT(IN) :: N ! the grid points (without ghost points)
DOUBLE PRECISION, INTENT(IN) :: mu ! material property, assume it to be constant 
DOUBLE PRECISION, DIMENSION(1:N,0:N+1), INTENT(OUT) :: tildeG
DOUBLE PRECISION, DIMENSION(6,8,8) :: Beta
INTEGER :: i,j,k
CALL STENCIL6(Beta)
tildeG = 0.d0
! ghost points
tildeG(1,0) = 12.d0/17.d0
tildeG(N,N+1) = 12.d0/17.d0
! first six rows and last six rows 
DO i = 1,6
  DO j = 1,8
    tildeG(i,j) = SUM(Beta(i,j,1:8))
    tildeG(N-(i-1),N-(j-1)) = SUM(Beta(i,j,1:8))
  END DO
END DO
! the middle rows
DO i = 7,N-6
  tildeG(i,i-1) = 16.d0/12.d0 
  tildeG(i,i-2) =-1.d0/12.d0
  tildeG(i,i) = -30.d0/12.d0
  tildeG(i,i+1) = 16.d0/12.d0
  tildeG(i,i+2) = -1.d0/12.d0
END DO
! here the mu is a constant, and we need to do extra effort if mu is a function
tildeG = mu*tildeG
! 
END SUBROUTINE SBP_gp_matrix
!
SUBROUTINE SBP_matrix(G,N,mu)
! construct the 4th order sbp operator for the second order derivative (without ghost point)
! G ~= (mu*u_x)_x
! G has the same form with tildeG except it does not have the first and the last column
! Specificly, the elements in G are the same with the elements in tildeG except the first and last rows
! G(1,1) = tildeG(1,1) + 12/17*mu_1*5           G(N,N) = tildeG(N,N) + 12/17*mu_N*5
! G(1,2) = tildeG(1,2) + 12/17*mu_1*(-10)       G(N,N-1) = tidleG(N,N-1) + 12/17*mu_N*(-10)
! G(1,3) = tildeG(1,3) + 12/17*mu_1*10          G(N,N-2) = tildeG(N,N-2) + 12/17*mu_N*10
! G(1,4) = tildeG(1,4) + 12/17*mu_1*(-5)        G(N,N-3) = tildeG(N,N-3) + 12/17*mu_N*(-5)
! G(1,5) = tildeG(1,5) + 12/17*mu_1*1           G(N,N-4) = tildeG(N,N-4) + 12/17*mu_N*1
DOUBLE PRECISION, DIMENSION(1:N,1:N), INTENT(OUT) :: G
DOUBLE PRECISION, INTENT(IN) :: mu
INTEGER, INTENT(IN) :: N
DOUBLE PRECISION, DIMENSION(1:N,0:N+1) :: tildeG
INTEGER :: i,j
G = 0.d0
CALL SBP_gp_matrix(tildeG,N,mu)
DO i = 1,N
  DO j = 1,N
    G(i,j) = tildeG(i,j)
  END DO
END DO
! extra treament for the first row and the last row of G
G(1,1) = tildeG(1,1) + 12.d0/17.d0*mu*5.d0
G(N,N) = tildeG(N,N) + 12.d0/17.d0*mu*5.d0
G(1,2) = tildeG(1,2) + 12.d0/17.d0*mu*(-10.d0)
G(N,N-1) = tildeG(N,N-1) + 12.d0/17.d0*mu*(-10.d0)
G(1,3) = tildeG(1,3) + 12.d0/17.d0*mu*10.d0
G(N,N-2) = tildeG(N,N-2) + 12.d0/17.d0*mu*10.d0
G(1,4) = tildeG(1,4) + 12.d0/17.d0*mu*(-5.d0)
G(N,N-3) = tildeG(N,N-3) + 12.d0/17.d0*mu*(-5.d0)
G(1,5) = tildeG(1,5) + 12.d0/17.d0*mu
G(N,N-4) = tildeG(N,N-4) + 12.d0/17.d0*mu
!
END SUBROUTINE SBP_matrix
!
END MODULE SBP4

